#from decorator import decorator
from django.shortcuts import get_object_or_404, render_to_response, RequestContext
from django.views.generic.simple import direct_to_template
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils import simplejson
from django.http import (HttpResponse, HttpResponseRedirect,
                         HttpResponseNotFound, Http404)
from profiles.models import Profile
from .models import Like
from .models import Viewed
from endless_pagination.decorators import page_templates
import datetime
from datetime import timedelta

#@decorator
def return_json(func, *args, **kwargs):
    result = func(*args, **kwargs)
    return HttpResponse(simplejson.dumps(result))


def get_default(queryset, rng):
    list1 = []
    if len(queryset) < rng:
        for i in range(rng):
            try:
                list1.append(queryset[i])
            except:
                list1.append(None)
        return list1
    else:
        return queryset

#@login_required
#@return_json
#def like(request, slug):
    #liked = get_object_or_404(Profile, slug=slug)
    #created, like = Like.objects.get_or_create(
        #user=request.user,
        #like=liked)
    #if created:
        #return {"created": created, "error": False}
    #return {"created": False, "error": "Already exists"}

#@login_required
@page_templates({
    "likes/likes_you_widget_page.html": 'widget_likes_you',
    "likes/you_likes_widget_page.html": 'widget_you_likes',
})
def get_likes_widget(request, template="likes/likes_widget.html", extra_context=None):
    if request.user.is_authenticated():
        qs1 = get_default(
            Like.objects.user_like(request.user).exclude(
                user__username = request.user.username
            ), 32)
        qs2 = get_default(Like.objects.like_user(request.user).exclude(
            like__username  = request.user.username
        ), 32)
        context = {
        'user':request.user,
        'likesyou': qs1,
        'youlikes': qs2,
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_BLOCK"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_BLOCK"),
        'linktoall': '/visits/likes/all/',
        }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)
    else:
        qs1 = get_default([], 32)
        qs2 = get_default([], 32)
        context = {
        'likesyou': qs1,
        'youlikes': qs2,
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_BLOCK"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_BLOCK"),
        'linktoall': '/visits/likes/all/',
        }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)



#@login_required
@page_templates({
    "likes/likes_you_all_page.html": 'all_likes_you',
    "likes/you_likes_all_page.html": 'all_you_likes',
})
def get_likes_all(request, template="likes/likes_all.html", extra_context=None):
    if request.user.is_authenticated():
        context = {
        'user':request.user,
        'likesyou': get_default(request.user.like_user.all(), 12),
            #Like.objects.user_like(request.user).exclude(
                #user__username = request.user.username
            #), 12),
        'youlikes': get_default(request.user.like.all(), 12),
            #Like.objects.like_user(request.user).exclude(
                #like__username = request.user.username
            #), 12),
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_PAGE"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_PAGE"),
        }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)
    else:
        context = {
        'likesyou': get_default([], 12),
        'youlikes': get_default([], 12),
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_PAGE"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_PAGE"),
        }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)



#@login_required
@page_templates({
    "viewed/you_viewed_widget_page.html": 'widget_you_viewed',
    "viewed/viewed_you_widget_page.html": 'widget_viewed_you',
})
def get_viewed_widget(request, template="viewed/viewed_widget.html", extra_context=None):
    if request.user.is_authenticated():
        list1 = []
        list2 = []
        qs1 = Viewed.objects.user_viewed(request.user).exclude(user__username = request.user.username)
        qs2 = Viewed.objects.viewed_user(request.user).exclude(viewed__username = request.user.username)
        for i in range(32):
            try:
                qs1[i]
                list1.append(qs1[i])
            except:
                list1.append(None)
        for i in range(32):
            try:
                qs2[i]
                list2.append(qs2[i])
            except:
                list2.append(None)
        context = {
        'user':request.user,
        'youviewed': list1,
        'viewedyou': list2,
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_BLOCK"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_BLOCK"),
        'linktoall': '/visits/viewed/all/',
        }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)
    else:
        list1 = []
        list2 = []
        qs1 = []
        qs2 = []
        for i in range(32):
            try:
                qs1[i]
                list1.append(qs1[i])
            except:
                list1.append(None)
        for i in range(32):
            try:
                qs2[i]
                list2.append(qs2[i])
            except:
                list2.append(None)
        context = {
        'youviewed': list1,
        'viewedyou': list2,
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_BLOCK"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_BLOCK"),
        'linktoall': '/visits/viewed/all/',
        }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)



#@login_required
@page_templates({
    "viewed/you_viewed_all_page.html": 'all_you_viewed',
    "viewed/viewed_you_all_page.html": 'all_viewed_you',
})
def get_viewed_all(request, template="viewed/viewed_all.html", extra_context=None):
    if request.user.is_authenticated():
        context = {
        'user':request.user,
        'youviewed': get_default(Viewed.objects.user_viewed(request.user).exclude(user__username = request.user.username), 100),
        'viewedyou': get_default(Viewed.objects.viewed_user(request.user).exclude(viewed__username = request.user.username), 100),
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_PAGE"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_PAGE"),
            }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)
    else:
        context = {
        'youviewed': get_default([], 100),
        'viewedyou': get_default([], 100),
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_PAGE"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_PAGE"),
            }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)




#@login_required
@page_templates({
    "viewed/online_widget.html": 'widget_online',
    })
def get_online_widget(request, template="viewed/online.html", extra_context=None):
    if request.user.is_authenticated():
        cur_user = Profile.objects.filter(last_active__gt = datetime.datetime.now() - timedelta(minutes = 15)).exclude(username = request.user.username)
        cur_list = []
        for i in range(32):
            try:
                cur_user[i]
                cur_list.append(cur_user[i])
            except:
                cur_list.append(None)
        context = {
        'user':request.user,
        'usersOnline': cur_list,
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_BLOCK"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_BLOCK"),
        'linktoall': '/visits/online/all/',
            }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)
    else:
        cur_user = Profile.objects.filter(last_active__gt = datetime.datetime.now() - timedelta(minutes = 15))
        cur_list = []
        for i in range(32):
            try:
                cur_user[i]
                cur_list.append(cur_user[i])
            except:
                cur_list.append(None)
        context = {
        'usersOnline': cur_list,
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_BLOCK"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_BLOCK"),
        'linktoall': '/visits/online/all/',
            }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)



#@login_required
@page_templates({
    "viewed/online_widget_all.html": 'widget_online_all',
})
def get_online_all(request, template="viewed/online_all.html", extra_context=None):
    if request.user.is_authenticated():
        context = {
        'user':request.user,
        'usersOnline': get_default(Profile.objects.filter(
            last_active__gt = (datetime.datetime.now() - timedelta(minutes=15))
        ).exclude(username = request.user.username), 100),
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_PAGE"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_PAGE"),
            }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)
    else:
        context = {
        'usersOnline': get_default(Profile.objects.filter(last_active__gt = datetime.datetime.now() - timedelta(minutes=15)),100),
        'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_PAGE"),
        'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_PAGE"),
            }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)

#@login_required
@page_templates({
    "viewed/registered_widget.html":'registered_widget',
    })
def get_registered_widget(request, template = "viewed/last_registered.html", extra_context = None):
    if request.user.is_authenticated():
        qs = Profile.objects.all().order_by('date_joined').exclude(username = request.user.username)
        list1 = []
        for i in range(32):
            try:
                qs[i]
                list1.append(qs[i])
            except:
                list1.append(None)
        context = {
            'user':request.user,
            'users_registered': list1,
            'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_BLOCK"),
            'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_BLOCK"),
            'linktoall':'/visits/registered/all',
            }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)
    else:
        qs = Profile.objects.all().order_by('date_joined')
        list1 = []
        for i in range(32):
            try:
                qs[i]
                list1.append(qs[i])
            except:
                list1.append(None)
        context = {
            'users_registered': list1,
            'numitem': getattr(settings, "COUNT_VISIBLE_ITEMS_BLOCK"),
            'numpag': getattr(settings, "COUNT_VISIBLE_PAGINATION_BLOCK"),
            'linktoall':'/visits/registered/all',
            }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)



#@login_required
@page_templates({
    "viewed/registered_widget_all.html":'registered_widget_all',
    })
def get_registered_all(request, template = 'viewed/last_registered_all.html', extra_context = None):
    if request.user.is_authenticated():
        context = {
            'user':request.user,
            'users_registered':get_default(Profile.objects.all().order_by('date_joined').exclude(username = request.user.username),100),
            'numitem':getattr(settings, "COUNT_VISIBLE_ITEMS_PAGE"),
            'numpag':getattr(settings, "COUNT_VISIBLE_PAGINATION_PAGE")
            }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)
    else:
        context = {
            'users_registered':get_default(Profile.objects.all().order_by('date_joined'),100),
            'numitem':getattr(settings, "COUNT_VISIBLE_ITEMS_PAGE"),
            'numpag':getattr(settings, "COUNT_VISIBLE_PAGINATION_PAGE")
            }
        if extra_context is not None:
            context.update(extra_context)
        return direct_to_template(request, template, context)




