(function($)    {
    function form_data(form)   {
        return form.find("input[checked], input[type='text'], input[type='hidden'], input[type='password'], input[type='submit'], option:selected, textarea").filter(':enabled');
    }
    function inputs(form)   {
        return form.find("input, select, textarea")
    }
 
 $.fn.last_submit_data = null;
    
    $.fn.submit_form = function(url, settings) {
        settings = $.extend({
            type: 'table',
            callback: false,
            fields: false,
        }, settings);
        var form = $(this);
 
        var params = {};
        
        form_data(form).each(function() {
            params[ this.name || this.id || this.parentNode.name || this.parentNode.id ] = this.value; 
        });
        
        var status = false;
        if (settings.fields) {
            params.fields = settings.fields;
        }
        console.log("form", form);
        var old = form.find("ul.errorlist");
        console.log(params);
        old.remove();
       
        $.ajax({
            async: false,
            data: params,
            dataType: 'json',
            error: function(XHR, textStatus, errorThrown)   {
                status = false;
            },
            success: function(data, textStatus) {
                status = data.saved;
                if (!status)    {

                if (settings.callback)  {
                    settings.callback(data, form);
                }
                else{
                    if (settings.type == 'p')    {
                        inputs(form).parent().prev('ul').remove();
                        inputs(form).parent().prev('ul').remove();
                        $.each(data.errors, function(key, val){
                            if (key == '__all__'){
                                var error = inputs(form).filter(':first').parent();
                                if (error.prev().is('ul.errorlist')) {
                                    error.prev().before('<ul class="errorlist"><li>' + val + '</li></ul>');
                                }
                                else{
                                    error.before('<ul class="errorlist"><li>' + val +'</li></ul>');
                                }
                            }
                            else{
                                $('#' + key).parent().before('<ul class="errorlist"><li>' + val + '</li></ul>');
                            }
                        });
                    }
                    if (settings.type == 'table')   {
                        inputs(form).prev('ul').remove();
                        inputs(form).filter(':first').parent().parent().prev('tr').remove();
                        $.each(data.errors, function(key, val)  {
                            if (key == '__all__')   {
                                inputs(form).filter(':first').parent().parent().before('<ul class="errorlist"><li>' + val + '.</li></ul>');
                            }
                            else{
                                $('#' + key).before('<ul class="errorlist"><li>' + val + '</li></ul>');
                            }
                        });
                    }
                    if (settings.type == 'ul')  {
                        inputs(form).prev().prev('ul').remove();
                        inputs(form).filter(':first').parent().prev('li').remove();
                        $.each(data.errors, function(key, val)  {
                            if (key == '__all__')   {
                                inputs(form).filter(':first').parent().before('<li><ul class="errorlist"><li>' + val + '</li></ul></li>');
                            }
                            else {
                                $('#' + key).prev().before('<ul class="errorlist"><li>' + val + '</li></ul>');
                            }
                        });
                    }
                }
                }
                $.fn.last_submit_data = data
                     },
            type: 'POST',
            url: url
        });
        return status;
    };
})(jQuery);
console.log("AJAX SUBMIT LOADED");


