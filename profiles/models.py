import hashlib
import random
import datetime
from os import path
from django.db import models
from django.contrib.auth.models import User, UserManager
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.conf import settings
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail import ImageField
#from subscribe.models import Subscribe
try:
    from notification import models as notification
except ImportError:
    notification = None

from .utils import datetime_to_epoch
#from locations.models import XCity
from cities_light.models import City


DEFAULT_ABOUT_ME = getattr(settings, 'DEFAULT_ABOUT_ME', 'Some simple text')


class Image(models.Model):
    '''This class represents user's avatar and all user's photos.
    hash - is 32 characters length string.
    first 16 chars - md5(self.id) - defines foolder name,
    next 16 - md5(self.upload_date as timestamp) - is file name.
    '''
    UPLOAD_TO = "images"

    def _get_hash(self):
        if not self.upload_date:
            self.upload_date = datetime.datetime.now()
        hsh = hashlib.md5(unicode(self.user.id))
        hsh.update(unicode(int(datetime_to_epoch(self.upload_date))))
        return hsh.hexdigest()


    def get_path(self, name):
        hsh = self._get_hash()
        extension = name.split('.')[-1]
        f_name = "." + extension
        self.slug = hsh
        return path.join(self.UPLOAD_TO, hsh[0:2], hsh[2:4] , hsh+f_name)

    slug = models.CharField(max_length=32, unique=True, db_index=True)
    is_avatar = models.BooleanField(default=False)
    is_safe = models.BooleanField(default=False)
    need_moderation = models.BooleanField(default=True)
    src = ImageField(verbose_name='', upload_to=get_path)
    upload_date = models.DateField(auto_now_add=True, auto_now=True)
    user = models.ForeignKey('Profile')

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_hash()
        super(Image, self).save(*args, **kwargs)


class ProfileManager(UserManager):
    def create_user(self, email, password=None, **required_fields):
        now = datetime.datetime.now()
        # create unique username
        import re
        re_name = r'^(?P<name>\w*?)(?P<count>\d+$)'  #r'^(?P<name>\D*)(?P<count>\d+?$)'
        username = email.split("@")[0]
        not_uniq = lambda uname: bool(self.model.objects.filter(username=uname).count())
        while not_uniq(username):
            match = re.match(re_name, username)
            if match:
                id = int(match.group("count"))
                id += 1
                username = "{}{}".format(match.group("name"), id)
            else:
                username = "{}{}".format(username, 1)

        hsh = hashlib.md5(unicode(datetime_to_epoch(datetime.datetime.now())))
        hsh.update(unicode(random.randint(1, 10**9)))
        required_fields.setdefault('slug', hsh.hexdigest())
        # maybe I need to filter **required_fields

        # Normalize the address by lowercasing the domain part of the email
        # address.
        try:
            email_name, domain_part = email.strip().split('@', 1)
        except ValueError:
            pass
        else:
            email = '@'.join([email_name, domain_part.lower()])

        user = self.model(username=username, email=email, is_staff=False,
                         is_active=True, is_superuser=False, last_login=now,
                         last_active = now,
                         date_joined=now, **required_fields)

        user.set_password(password)
        user.save(using=self._db)
        return user


class Profile(User):
    '''This class represents user's profile'''
    NOTIFICATION_CHOICES = (
        (0, 'No'),
        (1, 'Instant'),
        (2, 'Digest'),
    )
    EMAIL_VIEWS = (
        (0, 'No email notifications'),
        (1, 'Recieve emeail notification'),
    )
    THIRD_PARTY_EMAIL = (
        (0, 'No email notification'),
        (1, 'Recieve email notification'),
    )
    GENDER_CHOICES = (
        (0, 'Man'),
        (1, 'Woman'),
    )
    HEIGHT_CHOICES = (
        (0, "Prefer not to say"),
        (1, "&lt; 5 ft 0 in / &lt; 152 cm"),
        (2, "5 ft 0 in / 153-155 cm"),
        (3, "5 ft 1 in / 156-157 cm"),
        (4, "5 ft 2 in / 158-160 cm"),
        (5, "5 ft 3 in / 161-162 cm"),
        (6, "5 ft 4 in / 163-165 cm"),
        (7, "5 ft 5 in / 166-167 cm"),
        (8, "5 ft 6 in / 168-170 cm"),
        (9, "5 ft 7 in / 171-172 cm"),
        (10, "5 ft 8 in / 173-175 cm"),
        (11, "5 ft 9 in / 176-178 cm"),
        (12, "5 ft 10 in / 179-180 cm"),
        (13, "5 ft 11 in / 181-183 cm"),
        (14, "6 ft 0 in / 184-185 cm"),
        (15, "6 ft 1 in / 186-188 cm"),
        (16, "6 ft 2 in / 189-190 cm"),
        (17, "6 ft 3 in / 191-193 cm"),
        (18, "6 ft 4 in / 194-195 cm"),
        (19, "&gt; 6 ft 4 in / &gt; 195 cm"),
    )

    RACE_CHOICES = (
        (1, "Prefer not to say"),
        (2, "American Indian"),
        (3, "Asian"),
        (4, "Black"),
        (5, "Caucasian"),
        (6, "East Indian"),
        (7, "Hispanic"),
        (8, "Middle Eastern"),
        (9, "Various"),
        (10, "Other"),
    )

    RELIGION_CHOICES = (
        (1, "Prefer not to say"),
        (2, "Agnostic"),
        (3, "Atheist"),
        (4, "Buddhist/Taoist"),
        (5, "Catholic"),
        (6, "Hindu"),
        (7, "Islamic"),
        (8, "Jewish"),
        (9, "Protestant"),
        (10, "Spiritual"),
        (11, "Other"),
    )
    HAIR_COLOR_CHOICES = (
        (1, "Prefer not to say"),
        (2, "Black"),
        (3, "Brown"),
        (4, "Blond"),
        (5, "Auburn"),
        (6, "Red"),
        (7, "Gray"),
    )
    EYE_COLOR_CHOICES = (
        (1, "Prefer not to say"),
        (2, "Amber"),
        (3, "Blue"),
        (4, "Brown"),
        (5, "Gray"),
        (6, "Green"),
        (7, "Hazel"),
        (8, "Red"),
    )
    BODY_CHOICES = (
        (1, "Prefer not to say"),
        (2, "Average"),
        (3, "Slim"),
        (4, "Athletic"),
        (5, "Ample"),
        (6, "A little extra padding"),
        (7, "Large"),
    )
    PROFESSION_CHOICES = (
        (1, "Prefer not to say"),
        (2, "Not Working"),
        (3, "Student"),
        (4, "Engineering/Technical"),
        (5, "Business (Management)"),
        (6, "Clerical (Office/Shop)"),
        (7, "Design (Architect/Fashion)"),
        (8, "Education"),
        (9, "Government (Office/Shop)"),
        (10, "Other"),
        (11, "Medical (Nursing/Physician)"),
    )
    # location fields
    location = models.ForeignKey(City, blank=True, null=True)
    #lat = models.FloatField()
    #lng = models.FloatField()

    # slug = hash
    slug = models.SlugField(max_length=32)

    name = models.CharField(max_length=50)

    # settings fields
    email_notifications = models.IntegerField(
        choices=NOTIFICATION_CHOICES, default=1)
    email_profile_views = models.IntegerField(choices=EMAIL_VIEWS, default=1)
    third_party_email = models.IntegerField(choices=THIRD_PARTY_EMAIL,
                                            default=1)
    #Profile fields
    about_me = models.TextField("", blank=True)
    looking_for = models.IntegerField(choices=GENDER_CHOICES, default=1)
    height = models.IntegerField(choices=HEIGHT_CHOICES, default=0)
    race = models.IntegerField(choices=RACE_CHOICES, default=0)
    religion = models.IntegerField(choices=RELIGION_CHOICES, default=0)
    hair_color = models.IntegerField(choices=HAIR_COLOR_CHOICES, default=0)
    eye_color = models.IntegerField(choices=EYE_COLOR_CHOICES, default=0)
    body_type = models.IntegerField(choices=BODY_CHOICES, default=0)
    profession = models.IntegerField(choices=PROFESSION_CHOICES, default=0)
    birth_date = models.DateField(blank=True, null=True)
    gender = models.IntegerField(choices=GENDER_CHOICES, default=0)
    safe_mode = models.BooleanField(default=True)
    last_active = models.DateTimeField(blank=True, null=True)

    objects = ProfileManager()

    @models.permalink
    def get_absolute_url(self):
        return ('view_profile', None, {'slug': self.slug})

    def save(self, *args, **kwargs):
        if not self.name and self.email:
            self.name = self.email.split("@")[0]
        if not self.slug:
            hsh = hashlib.md5(unicode(datetime_to_epoch(datetime.datetime.now())))
            hsh.update(unicode(random.randint(1, 10**9)))
            self.slug = hsh.hexdigest()
        super(Profile, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.username

    def my_age(self):
        return (datetime.datetime.now().date() - self.birth_date).days/365

    def get_images(self):
        images = [None for i in xrange(9)]
        for i, img in enumerate(self.image_set.all().order_by("-is_avatar", "-upload_date")[:9]):
            images[i] = img
        return images

    def get_avatar(self):
        try:
            return self.image_set.get(is_avatar=True, need_moderation=False)
        except Image.DoesNotExist:
            return
        except Image.MultipleObjectsReturned:
            return  self.image_set.filter(is_avatar=True, need_moderation=False).order_by('-upload_date')[0]

    def is_paid(self):
        return bool(self.subscribe_set.count())

    def is_online_now(self):
        if self.last_active:
            return (datetime.datetime.now() - self.last_active).seconds < 10 * 60
        return False






class Update(models.Model):
    EVENT_TYPE_CHOICES = (
        (0, 'new user'),
        (1, 'uploaded new picture'),
        (2, 'new message'),
        (3, 'changed profile picture'),
    )
    def _get_hash(self):
        if not self.event_date:
            self.event_date = datetime.datetime.now()
        hsh = hashlib.md5(unicode(self.user.id))
        hsh.update(unicode(int(datetime_to_epoch(self.event_date))))
        return hsh.hexdigest()


    slug = models.SlugField(max_length=32)
    event_date = models.DateTimeField(auto_now_add=True, auto_now=True)
    event_type = models.IntegerField(choices=EVENT_TYPE_CHOICES)
    message = models.TextField(blank=True)
    user = models.ForeignKey(Profile)

    class Meta:
        ordering = ("-event_date", "event_type")

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_hash()
        return super(Update, self).save(*args, **kwargs)


    def when(self):
        delta = datetime.datetime.now() - self.event_date
        months = delta.days/30
        weeks = delta.days/7
        days = delta.days
        hours = delta.seconds/60/60
        return {'month': months, 'week': weeks, 'day': days, 'hour': hours}


@receiver(post_save, sender=Profile, dispatch_uid='profile_created')
def profile_created_handler(sender, **kwargs):
    profile = kwargs.get('instance', None)
    if kwargs.get('created', False):
        created, instance = Update.objects.get_or_create(
            message="Become a new member!",
            event_type=0,
            user=profile)
        if notification:
            # send notification
            try:
                notification.send([profile], "profile_created", {"user": profile})
            except Exception as e:
                pass

@receiver(post_save, sender=Image, dispatch_uid="profile_updated")
def picture_uploaded_handler(sender, **kwargs):
    img = kwargs['instance']
    if kwargs.get('created', False):
        # convert image to max size
        delta = datetime.datetime.now() - datetime.timedelta(
            seconds=getattr(settings, "MIN_PROFILE_UPDATE_INTERVAL", 10))
        if not Update.objects.filter(event_date__gte=delta,
                                user=img.user,
                                event_type=1):
            Update.objects.create(
                event_type = 1,
                user = img.user,
                message = 'Uploaded new picture!')
            if notification:
                # send notification
                notification.send([img.user], "image_uploaded", {"user": img.user})


@receiver(post_save, sender=User, dispatch_uid="User_created")
def create_profile_for_user(sender, **kwargs):
    if kwargs.get('created', False):
        user = kwargs.get('instance')
        from django.db import connection
        if Profile._meta.db_table in connection.introspection.table_names():
            f_values = dict((f.name, getattr(user, f.name)) for f in user._meta.fields)
            p = Profile(**f_values)
            p.user_ptr = user
            p.save()




