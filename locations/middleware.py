
stub = {
    'GEOIP_COUNTRY_CODE': 'US',
    'GEOIP_COUNTRY_CODE3': 'USA',
    'GEOIP_COUNTRY_NAME': 'United States',
    'GEOIP_CITY_COUNTRY_CODE': 'US',
    'GEOIP_CITY_COUNTRY_CODE3': 'USA',
    'GEOIP_CITY_COUNTRY_NAME': 'United States',
    'GEOIP_REGION': 'CA',
    'GEOIP_CITY': 'Aliso Viejo',
    'GEOIP_POSTAL_CODE': '92656',
    'GEOIP_CITY_CONTINENT_CODE': 'NA',
    'GEOIP_LATITUDE': '33.5717',
    'GEOIP_LONGTITUDE': '-117.7289',
    'GEOIP_DMA_CODE': '803',
    'GEOIP_AREA_CODE': '949',
}

class LocationStubMiddleware(object):
    def process_request(self, request):
        request.META.update(stub)

class LocationMiddleware(object):
    def process_request(self, request):
        from cities_light import City, Country, Region
        request.location = {}
        request.location['city'] = City.objects.get(name='GEOIP_CITY')
        request.location['region'] = Region.objects.get(name='GEOIP_REGION')
        request.location['country'] = Country.objects.get(name='GEOIP_CITY_COUNTRY_NAME')
