#! /bin/sh

### BEGIN INIT INFO
# Provides:          nginx
# Required-Start:    $local_fs $remote_fs $network $syslog
# Required-Stop:     $local_fs $remote_fs $network $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: starts the nginx web server
# Description:       starts nginx using start-stop-daemon
### END INIT INFO

USER=www-data
GROUP=www-data

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/home/ubuntu/django/test/xdc/manage.py
NAME=xdating
DESC=xdating

SOCKET=/home/ubuntu/django/test/xdc/run/$NAME.sock
PIDFILE=/home/ubuntu/django/test/xdc/run/$NAME.pid

# Web socket
HOST='0.0.0.0'
PORT=3033
DAEMON_OPTS="runfcgi method=prefork daemonize=false pidfile=${PIDFILE} host=${HOST} port=${PORT}"

# Unix socket
#DAEMON_OPTS="runfcgi method=prefork daemonize=true pidfile=${PIDFILE} socket=${SOCKET} "

#test -x $DAEMON || exit 0

#set -e
#. /lib/lsb/init-functions

start() {
# Starts daemon
	echo -n "Starting $DESC: "
	start-stop-daemon --start --quiet --pidfile $PIDFILE\
		--exec $DAEMON -- $DAEMON_OPTS  || true
	echo "$NAME."
#	chown $USER:$GROUP $SOCKET
}

stop() {
# Stops daemon
	echo -n "Stopping $DESC: "
	kill `cat ${PIDFILE}`
	rm $PIDFILE
#	start-stop-daemon --stop --quiet --pidfile $PIDFILE\
#		--exec $DAEMON || true
	echo "$NAME."

}

case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  restart)
	stop
	start
        ;;
  status)
	status_of_proc -p /var/run/$NAME.pid "$DAEMON" xdating && exit 0 || exit $?
	;;
  *)
	echo "Usage: $NAME {start|stop|restart}" >&2
	exit 1
	;;
esac

exit 0

