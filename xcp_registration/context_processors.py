from xcp_registration.forms import RegistrationFormStep1
from django.contrib.auth.forms import AuthenticationForm
import datetime
from xcp_registration.forms import now
from django.contrib.auth.forms import PasswordResetForm

def reg_auth_forms(request):
    return {
        "registration_form_step_1": RegistrationFormStep1(
            initial = {'birth_date': datetime.datetime(now -27 ,1,1), 'tos':True}),
        "authentication_form": AuthenticationForm(),
        "password_reset_form": PasswordResetForm(),

    }
