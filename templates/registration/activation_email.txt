Activation account on {{ site }}.


You should activate your account in {{expiration_days}} days.

Please follow this link to activate account right now:
http://{{ site }}{% url registration_activate activation_key %}

